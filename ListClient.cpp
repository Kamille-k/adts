#include <iostream>
#include "List.h"

using namespace std;

int main()
{

 List L1, L2; //Declare two list objects, L1 and L2


 cout << "Welcome to my List ADT client"<<endl<<endl;    //cout

 //Do some stuff with L1, L2, ... (Eg. cout<<L1.size(); )
 // ...
 
 //cout << L2.size();  
  
 L1.insert(2,1);
 L1.insert(5,2);
 L1.insert(7,3);

cout<<L1.get(2)<<endl;
cout<<L1.get(1)<<endl;
cout<<L1.get(3)<<endl;

L1.remove(2);
cout<<L1.size()<<endl;
cout<<L1.get(2)<<endl;
cout<<L1.get(1)<<endl;

}
