#include "StackV.h"
#include <iostream>
#include <vector>

using namespace std;
void Stack::push(int num)
{
	data.push_back(num);
}
int Stack::size()
{
	return data.size();
}
int Stack::top()
{
	return data[data.size()-1];
}
void Stack::pop()
{
	data.pop_back();
}
void Stack::clear()
{
	while(size() > 0)
	pop();
}
